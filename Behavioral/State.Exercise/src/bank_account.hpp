#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>

namespace Bank
{
    class InsufficientFunds : public std::runtime_error
    {
        const int id_;

    public:
        InsufficientFunds(const std::string& msg, int id) : std::runtime_error{msg}, id_{id}
        {
        }

        int id() const
        {
            return id_;
        }
    };

    enum AccountState
    {
        overdraft,
        normal
    };

    class BankAccount
    {
        int id_;
        double balance_;
        
        class AccountState
        {
        public:
            virtual void withdraw(BankAccount& ba, double amount) const = 0;            
            virtual void pay_interest(BankAccount& ba) const = 0;
            virtual std::string state() const = 0;
            virtual ~AccountState() = default;            
        };

        class NormalState : public AccountState
        {
        public:
            void withdraw(BankAccount& ba, double amount) const override
            {
                ba.balance_ -= amount;
            }            
            
            void pay_interest(BankAccount& ba) const override
            {
                ba.balance_ += ba.balance_ * 0.05;
            }

            std::string state() const override
            {
                return "normal";
            }
        };

        class OverdraftState : public AccountState
        {
            void withdraw(BankAccount& ba, double amount) const override
            {           
                throw InsufficientFunds{"Insufficient funds for account #" + std::to_string(ba.id_), ba.id_};     
            }
                        
            void pay_interest(BankAccount& ba) const override
            {
                ba.balance_ += ba.balance_ * 0.15;
            }

            std::string state() const override
            {
                return "overdraft";
            }
        };

        inline const static NormalState normal_state_;
        inline const static OverdraftState overdraft_state_;

        const AccountState* state_ = &normal_state_;

    protected:
        void update_account_state()
        {
            if (balance_ < 0)
                state_ = &overdraft_state_;
            else
                state_ = &normal_state_;
        }

        void set_balance(double amount)
        {
            balance_ = amount;
        }

    public:
        BankAccount(int id) : id_(id), balance_(0.0)
        {}

        void withdraw(double amount)
        {
            assert(amount > 0);

            state_->withdraw(*this, amount);

            update_account_state();
            
        }

        void deposit(double amount)
        {
            assert(amount > 0);

            balance_ += amount;

            update_account_state();
        }

        void pay_interest()
        {
            state_->pay_interest(*this);
        }

        std::string status() const
        {
            std::stringstream strm;
            strm << "BankAccount #" << id_ << "; State: " << state_->state() << "; "
                 << "Balance: " << std::to_string(balance_);

            return strm.str();
        }

        double balance() const
        {
            return balance_;
        }

        int id() const
        {
            return id_;
        }
    };
}

#endif
