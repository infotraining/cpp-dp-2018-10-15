#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

struct StatResult
{
    std::string description;
    double value;

    StatResult(const std::string& desc, double val)
        : description(desc)
        , value(val)
    {
    }
};

using Data = std::vector<double>;
using Results = std::vector<StatResult>;

// enum StatisticsType
// {
//     avg,
//     min_max,
//     sum
// };

using Statistics = std::function<Results(const Data&)>;

struct Avg
{
    Results operator()(const Data& data)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        return {{"AVG", avg}};
    }
};

struct MinMax
{
    Results operator()(const Data& data)
    {
        auto [it_min, it_max] = std::minmax_element(data.begin(), data.end());    

        return {{"MIN", *it_min}, {"MAX", *it_max}};
    }
};

struct Sum
{
    Results operator()(const Data& data)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        return {{"SUM", sum}};
    }
};

struct StatGroup
{
    std::vector<Statistics> stats_;

    explicit StatGroup(std::initializer_list<Statistics> stats) : stats_{stats}
    {    
    }

    void add(Statistics stat)
    {
        stats_.push_back(stat);
    }

    Results operator()(const Data& data)
    {
        Results results;

        for(const auto& stat : stats_)
        {
            auto stat_results = stat(data);

            std::move(std::begin(stat_results), std::end(stat_results), std::back_inserter(results));
        }

        return results;
    }
};

class DataAnalyzer
{
    Statistics statistics_;
    Data data_;
    Results results_;

public:
    DataAnalyzer(Statistics stat)
        : statistics_{stat}
    {
    }

    void load_data(const std::string& file_name)
    {
        data_.clear();
        results_.clear();

        std::ifstream fin(file_name.c_str());
        if (!fin)
            throw std::runtime_error("File not opened");

        double d;
        while (fin >> d)
        {
            data_.push_back(d);
        }

        std::cout << "File " << file_name << " has been loaded...\n";
    }

    void set_statistics(Statistics stat)
    {
        statistics_ = stat;
    }

    void calculate()
    {
        auto stat_results = statistics_(data_);

        std::move(std::begin(stat_results), std::end(stat_results), std::back_inserter(results_));
    }

    const Results& results() const
    {
        return results_;
    }
};

void show_results(const Results& results)
{
    for (const auto& rslt : results)
        std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
    Avg avg;
    MinMax min_max;
    Sum sum;

    StatGroup std_stats{ avg, min_max, sum };

    DataAnalyzer da{std_stats};
    da.load_data("data.dat");
    da.calculate();    

    show_results(da.results());

    std::cout << "\n\n";

    da.load_data("new_data.dat");
    da.set_statistics(StatGroup{MinMax{}, Sum{}});
    da.calculate();

    show_results(da.results());
}
