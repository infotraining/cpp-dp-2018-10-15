#ifndef SINGLETON_HPP
#define SINGLETON_HPP

#include <string>

template <typename T>
class SingletonHolder
{
    SingletonHolder() = default;

    SingletonHolder(const SingletonHolder&) = delete;
    SingletonHolder& operator=(const SingletonHolder&) = delete;

    static T& instance()
    {
        static T unique_instance;

        return unique_instance;
    }
};

namespace BadImpl
{
    template <typename T>
    class SingletonHolder
    {
        static T* instance_;
        SingletonHolder() = default;

        SingletonHolder(const SingletonHolder&) = delete;
        SingletonHolder& operator=(const SingletonHolder&) = delete;

        static T& instance()
        {
            if (instance_ == nullptr)
                instance_ = new T();

            return *instance_;
        }
    };

    template <typename T>
    T SingletonHolder::instance_ = nullptr;
}

#endif
