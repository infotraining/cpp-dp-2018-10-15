#include <cstdlib>
#include <iostream>
#include <list>
#include <string>
#include <unordered_map>
#include <vector>

#include "factory.hpp"

using namespace std;

namespace Canonical
{

    class Service
    {
        shared_ptr<LoggerCreator> creator_;

    public:
        Service(shared_ptr<LoggerCreator> creator)
            : creator_(creator)
        {
        }

        Service(const Service&) = delete;
        Service& operator=(const Service&) = delete;

        void use()
        {
            unique_ptr<Logger> logger = creator_->create_logger();
            logger->log("Client::use() has been started...");
            run();
            logger->log("Client::use() has finished...");
        }

    protected:
        virtual void run() {}
    };

    using LoggerFactory = std::unordered_map<std::string, shared_ptr<LoggerCreator>>;
}

class Service
{
    LoggerCreator creator_;

public:
    Service(LoggerCreator creator)
        : creator_(creator)
    {
    }

    Service(const Service&) = delete;
    Service& operator=(const Service&) = delete;

    void use()
    {
        unique_ptr<Logger> logger = creator_();
        logger->log("Client::use() has been started...");
        run();
        logger->log("Client::use() has finished...");
    }

protected:
    virtual void run() {}
};

namespace AlternativeTake
{
    class Service
    {    
    public:
        Service(const Service&) = delete;
        Service& operator=(const Service&) = delete;

        void use()
        {
            unique_ptr<Logger> logger = create_logger();
            logger->log("Client::use() has been started...");
            run();
            logger->log("Client::use() has finished...");
        }

    protected:
        virtual void run() {}

        virtual unique_ptr<Logger> create_logger()
        {
            return std::make_unique<ConsoleLogger>();
        }
    };
}

using LoggerFactory = std::unordered_map<std::string, LoggerCreator>;

std::string load_from_config()
{
    return "DbLogger";
}

int main()
{
    LoggerFactory logger_factory;
    logger_factory.insert(make_pair("ConsoleLogger", &make_unique<ConsoleLogger>));
    logger_factory.insert(make_pair("FileLogger", FileLoggerCreator{"log.dat"}));
    logger_factory.insert(make_pair("DbLogger", [] { return make_unique<DbLogger>("https://db.com"); }));

    string logger_id = load_from_config();

    Service srv(logger_factory.at(logger_id));
    srv.use();

    // Iterator pattern in STL
    // begin() as Factory Method

    list<int> vec = {1, 2, 3};

    for (const auto& item : vec)
    {
        cout << item << endl;
    }

    for (auto it = vec.begin(); it != vec.end(); ++it) // vec.begin() - factory method
    {
        const auto& item = *it;
        cout << item << endl;
    }
}
