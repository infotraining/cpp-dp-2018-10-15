#include "bitmap.hpp"
#include <algorithm>
#include <array>
#include <vector>

using namespace std;

struct Bitmap::BitmapImpl
{
    std::vector<char> image_;
};

Bitmap::Bitmap(size_t size, char fill_char) 
    : impl_{std::make_unique<BitmapImpl>()}
{
    impl_->image_.resize(size);
    fill_n(begin(impl_->image_), size, fill_char);
}

Bitmap::~Bitmap() = default;

void Bitmap::draw()
{
    cout << "Image: ";
    for (char pixel : impl_->image_)
        cout << pixel;
    cout << endl;
}
