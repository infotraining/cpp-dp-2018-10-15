#include "starbugs_coffee.hpp"
#include <memory>
#include <cassert>

void client(std::unique_ptr<Coffee> coffee)
{
    std::cout << "Description: " << coffee->get_description() << "; Price: " << coffee->get_total_price() << std::endl;
    coffee->prepare();
}

class CoffeeBuilder
{
    CoffeePtr coffee_;
public:
    template <typename BaseType>
    CoffeeBuilder& create_base()
    {
        coffee_ = std::make_unique<BaseType>();

        return *this;
    }

    template <typename CondimentType>
    CoffeeBuilder& add()
    {
        assert(coffee_ != nullptr);

        coffee_ = std::make_unique<CondimentType>(std::move(coffee_));

        return *this;
    }

    CoffeePtr get_coffee()
    {
        return std::move(coffee_);
    }
};

int main()
{
    // std::unique_ptr<Coffee> cf = 
    //     std::make_unique<WhippedCream>(
    //         std::make_unique<Whisky>(
    //             std::make_unique<Whisky>(
    //                 std::make_unique<Espresso>())));

    CoffeeBuilder cb;
    cb.create_base<Espresso>()
        .add<Whisky>()
        .add<Whisky>()
        .add<WhippedCream>();

    client(cb.get_coffee());

    // CoffeeBuilder cb;
    // cb.create_base<Espresso>()
    //     .add<Whisky, Whisky, WhippedCream>();
}
