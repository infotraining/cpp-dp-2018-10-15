get_filename_component(PROJECT_NAME_STR ${CMAKE_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${PROJECT_NAME_STR})

cmake_minimum_required(VERSION 3.6)
project(${PROJECT_NAME_STR})

#----------------------------------------
# Setting a compiler
#----------------------------------------
if (MSVC)
    # warning level 4 and all warnings as errors
    add_compile_options(-D_SCL_SECURE_NO_WARNINGS)
else()
    # lots of warnings and all warnings as errors
    add_compile_options(-Wall)
endif()

#----------------------------------------
# Boost headers
#----------------------------------------
find_package(Boost 1.62.0)
if(Boost_FOUND)
	include_directories(${Boost_INCLUDE_DIRS})
endif()

#----------------------------------------
# set Threads
#----------------------------------------
find_package(Threads REQUIRED)

#----------------------------------------
# Application
#----------------------------------------
aux_source_directory(. SRC_LIST) # Source files
file(GLOB HEADERS_LIST "*.h" "*.hpp") # Header files
add_executable(${PROJECT_NAME} ${SRC_LIST} ${HEADERS_LIST})
target_link_libraries(${PROJECT_NAME} ${CMAKE_THREAD_LIBS_INIT}) 
target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_14)